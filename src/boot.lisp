(defpackage #:niboot/boot
  (:use :cl)
  (:export on
           minimal-setup
           setup

           *init*
           *libecl*))

(in-package #:niboot/boot)

(defvar *prefix* nil)

(defvar *essential-libraries* '("libatomic_ops.so.1"
                                "libc.so.6"
                                "libdl.so.2"
                                "libm.so.6"
                                "libpthread.so.0"
                                "libffi.so.6"
                                "libgmp.so.10"))

(defvar *elf-interpreter* #p"/lib64/ld-linux-x86-64.so.2")
(defvar *relative-elf-interpreter* #p"lib64/ld-linux-x86-64.so.2")

(defvar *default-folders* '(#p"/dev/"
                            #p"/proc/"
                            #p"/sys/"
                            #p"/original_root/"
                            #p"/final_root/"))

(defvar *root-device-uuid* nil)
(defvar *boot-device-uuid* nil)

(defun mp (&rest args)
  (apply #'uiop:merge-pathnames* args))

(defun cat (&rest args)
  (apply #'concatenate 'string args))

(defun on (image uuids &optional (switch-root t))
  "Boot on an image.

To boot on an image, everything must be prepared before we switch_root
and end up in the image's init process.

1. Mount swap if available.
2. Mount the squashfs.
3. Mount the /boot partition if applicable.
4. Bind-mount the /boot folder if applicable.
5. Bind-mount the kernel modules folders.
6. Bind-mount /var/log/, /var/cache and /var/lib/.
7. Mount /tmp/ and /run/ as tmpfs.
8. Write the custom /run/.niboot/fstab.
"
  ;; We really want to start mounting swap ASAP, because we're then
  ;; copying files in rootfs, which is an in-memory filesystem. Then
  ;; mounting the squashfs, which is also an in-memory filesystem.
  (dolist (fstab-entry (niboot/util:parse-fstab #P"/original_root/etc/fstab"))
    (when (string= "swap" (niboot/util:fstab-type fstab-entry))
      (niboot/util:swapon (niboot/util:fstab-device fstab-entry))))

  ;; Kernel modules to mount a squashfs
  (niboot/util:modprobe "loop")
  (niboot/util:modprobe "squashfs")

  ;; Copy to the rootfs so that we can later umount /original_root/
  (alexandria:copy-file (niboot/database:archive image)
                        (niboot/database:name image))

  (niboot/util:mount "squashfs"
                     (niboot/database:name image)
                     "/final_root/")

  (if (jsown:keyp uuids :boot-device-uuid)
      ;; TODO: don't hardcode the fs
      (niboot/util:mount "ext4"
                         (format nil "UUID=~s" (jsown:val uuids :boot-device-uuid))
                         "/final_root/boot/")
      (niboot/util:bind-mount "/original_root/boot/" "/final_root/boot/"))

  ;; TODO: only bind-mount the kernel-specific folder.
  (dolist (folder '(#p"lib/modules/" #p"usr/lib/modules"))
    (when (probe-file (mp folder #p"/original_root/"))
      (niboot/util:bind-mount (namestring (mp folder #p"/original_root/"))
                              (namestring (mp folder #p"/final_root/")))))

  (dolist (volume-map (niboot/database:volumes-maps image))
    (let ((source-path (mp (niboot/database:source-path volume-map) #p"/original_root/")))
      (ensure-directories-exist source-path)
      (niboot/util:bind-mount (namestring source-path)
                              (namestring
                               (mp (niboot/database:destination-path volume-map)
                                   #p"/final_root/")))))

  (niboot/util:umount "/original_root/")

  (dolist (temporary-volume (niboot/database:temporary-volumes image))
    (niboot/util:mount "tmpfs" "tmpfs" (cat "/final_root" temporary-volume)))

  (ensure-directories-exist #p"/final_root/run/.niboot/")
  (niboot/util:output (format
                       nil
                       "
~a
~{tmpfs ~a tmpfs defaults 0 0~%~}
"
                       (if (string= (jsown:val uuids :boot-device-uuid)
                                    (jsown:val uuids :root-device-uuid))
                           ""
                           (format nil "UUID=~a /boot ext4 defaults 0 2"
                                   (jsown:val uuids :boot-device-uuid)))
                       (niboot/database:temporary-volumes image))
                      #p"/final_root/run/.niboot/fstab")

  (when switch-root
    (niboot/util:exec "/bin/busybox" "switch_root" "/final_root/" "/sbin/init")))

(defun minimal-setup (uuids)
  (uiop:ensure-all-directories-exist *default-folders*)

  ;; Some essential folders need to be mounted.
  (niboot/util:mount "proc" "none" "/proc")
  (niboot/util:mount "sysfs" "none" "/sys")
  (niboot/util:mount "devtmpfs" "none" "/dev")

  ;; Minimal kernel modules for /dev/sd* devices to appear.
  (niboot/util:modprobe "sd_mod")
  (niboot/util:modprobe "ata_piix")

  ;; Necessary to mount an ext4 fs.
  (niboot/util:modprobe "ext4")

  ;; Mount the original root device on /original_root
  ;; TODO: don't hardcode the fs
  ;; TODO: Decrypt an LUKS encrypted disk if necessary. This is possible like this:
  ;; - lsblk should tell you if a partition is of type "crypt"
  ;; - If yes, you can map the kernel command line parameter "rd.luks.uuid"
  ;;   and the blk name. (Remember to write out the kernel parameters during niboot setup.)
  ;; - Decrypt with "cryptsetup luksOpen <device name> <luks uuid>". This should give you
  ;;   a /dev/mapper/<luks uuid> device.
  ;; - Run lvscan to make sure at least the kernel parameter "rd.lvm.lv" LV is
  ;;   loaded. (Not sure if this is necessary.)
  ;; - Load whatever root= is. Or :root-device-uuid. Not sure if they match.
  (niboot/util:mount "ext4"
                     (format nil "UUID=~s" (jsown:val uuids :root-device-uuid))
                     "/original_root"))

(defun setup (build-prefix)
  (setf *prefix* build-prefix)

  ;; Setting up grub must be done before writing the initramfs because
  ;; we're fetching information like the devices UUIDs that will need
  ;; to be in the initramfs image.
  (setup-grub)
  (setup-initramfs))

(defun copy-kernel-files (temp-dir kernel-version)
  (let ((kernel-modules-src (make-pathname
                             :directory `(:absolute
                                          "lib"
                                          "modules"
                                          ,kernel-version)))
        (kernel-modules-dst (mp
                             (make-pathname
                              :directory `(:relative
                                           "lib"
                                           "modules"
                                           ,kernel-version))
                             temp-dir)))
    (ensure-directories-exist kernel-modules-dst)

    ;; It really sucks to shell out there, but the equivalent in Lisp
    ;; is like 50 lines.
    (uiop:run-program (list "rsync"
                            "-a"
                            (namestring kernel-modules-src)
                            (namestring kernel-modules-dst))
                      :output t)))

(defun write-init (temp-dir)
  (ensure-directories-exist (mp #p"bin/" temp-dir))
  (dolist (file (directory
                 (mp (make-pathname :directory '(:relative
                                                 "share"
                                                 "niboot"
                                                 "resources")
                                    :name :wild
                                    :type :wild)
                     *prefix*)))
    (let ((file-path (mp
                      (make-pathname
                       :directory '(:relative
                                    "bin")
                       :name (pathname-name file)
                       :type (pathname-type file))
                      temp-dir)))
      (alexandria:copy-file file file-path)
      (niboot/util:chmod file-path #o755)))

  ;; We need to fetch our essential libraries on the given system,
  ;; otherwise the kernel won't find whatever is linked in the init
  ;; binary.
  (dolist (library *essential-libraries*)
    ;; TODO: optimize this to have a single 'ldconfig -p'
    (let ((library-path (niboot/util:find-library-path library)))
      (when library-path
        (let ((relative-library-path (niboot/util:absolute-to-relative library-path)))
          (ensure-directories-exist (mp
                                     (make-pathname :directory (pathname-directory
                                                                relative-library-path))
                                     temp-dir))
          (alexandria:copy-file library-path (mp
                                              relative-library-path
                                              temp-dir))))))

  ;; Both the init and the ELF interpreter need to be executable.
  (ensure-directories-exist (mp
                             (make-pathname :directory (pathname-directory *relative-elf-interpreter*))
                             temp-dir))
  (alexandria:copy-file *elf-interpreter* (mp *relative-elf-interpreter*
                                              temp-dir))
  (niboot/util:chmod (mp *relative-elf-interpreter*
                         temp-dir)
                     #o755)

  (ensure-directories-exist (mp #p"lib/" temp-dir))
  (alexandria:copy-file (mp #p"lib/libecl.so.16.1" *prefix*)
                        (mp #p"lib/libecl.so.16.1" temp-dir))

  (alexandria:copy-file (mp #p"share/niboot/help.doc" *prefix*)
                        (mp #p"help.doc" temp-dir))

  (alexandria:copy-file (mp #p"share/niboot/init" *prefix*)
                        (mp #p"init" temp-dir))
  (niboot/util:chmod (mp #p"init" temp-dir)
                     #o755)

  ;; Finally, we store the UUIDs of the root and boot devices.
  (with-open-file (f (mp #p"uuids" temp-dir) :direction :output)
    (write-string (jsown:to-json `(:obj
                                   (:boot-device-uuid . ,*boot-device-uuid*)
                                   (:root-device-uuid . ,*root-device-uuid*)
                                   (:kernel-cmdline . ,(niboot/util:kernel-cmdline))))
                  f)))

(defun write-cpio (destination)
  (uiop:run-program
   `("bash"
     "-c"
     ,(format nil "find . | cpio -o --format=newc | gzip -9 > ~a" (namestring destination)))))

(defun setup-initramfs ()
  (niboot/util:with-temporary-directory-and-chdir (temp-dir)
    ;; TODO: add support for 'niboot setup' KERNEL-VERSION argument.
    (copy-kernel-files temp-dir (niboot/util:current-kernel-version))

    (write-init temp-dir)

    (write-cpio #p"/boot/niboot.cpio.gz")))

(defun setup-grub ()
  (multiple-value-bind (root-device-uuid boot-device-uuid)
      (find-devices-uuid)
    (setf *boot-device-uuid* boot-device-uuid)
    (setf *root-device-uuid* root-device-uuid))

  ;; TODO: add support for 'niboot setup' KERNEL-VERSION argument.
  (let* ((kernel-version (niboot/util:current-kernel-version))
         (kernel-binary (format nil "vmlinuz-~a" kernel-version))
         (is-efi (probe-file #p"/sys/firmware/efi/")))
    (unless (probe-file (mp kernel-binary #p"/boot/"))
      (error (format nil "Unable to find ~a" kernel-binary)))

    (let ((custom-grub-file #p"/etc/grub.d/99_niboot"))
      (with-open-file (f custom-grub-file
                         :direction :output
                         :if-exists :overwrite
                         :if-does-not-exist :create)
        (write-grub-file f kernel-version))
      (niboot/util:chmod custom-grub-file #o755))

    (if (probe-file #p"/usr/sbin/update-grub")
        ;; Debian and derivatives
        (uiop:run-program '("update-grub"))
        ;; CentOS and derivatives
        (uiop:run-program '("grub2-mkconfig") :output (find-grub-config-file is-efi)))))

(defun find-devices-uuid ()
  (let ((root-device-uuid nil)
        (boot-device-uuid nil))
    (dolist (mount (niboot/util:mounts))
      (when (equal #p"/" (niboot/util:where mount))
        (setf root-device-uuid (niboot/util:block-device-property "UUID" (niboot/util:what mount))))
      (when (equal #p"/boot/" (niboot/util:where mount))
        (setf boot-device-uuid (niboot/util:block-device-property "UUID" (niboot/util:what mount)))))
    (values root-device-uuid
            (if boot-device-uuid
                boot-device-uuid
                ;; Fallback if /boot isn't a separate partition
                root-device-uuid))))

(defun find-grub-config-file (is-efi)
  (if is-efi
      #p"/etc/grub2-efi.cfg"
      #p"/etc/grub2.cfg"))

(defun write-grub-file (grub-file-stream kernel-version)
  ;; TODO: Fedora wants "linuxefi" while Debian wants "linux". Figure
  ;; out how to properly fix that.
  (format
   grub-file-stream
   "#!/bin/sh
exec tail -n +3 $0

menuentry 'niboot on Linux ~a' --class gnu-linux --class gnu --class os {
  load_video
  insmod gzio
  insmod part_msdos
  insmod ext2
  search --no-floppy --fs-uuid --set=root ~a
  echo 'Loading Linux ~a...'
  set gfxpayload=1024x768x24,1024x768
  linuxefi ~a/vmlinuz-~a root=UUID=~a
  echo 'Loading niboot menu...'
  initrdefi ~a/niboot.cpio.gz
}
" kernel-version *boot-device-uuid* kernel-version
(if (string= *boot-device-uuid* *root-device-uuid*) "/boot" "")
kernel-version *root-device-uuid*
(if (string= *boot-device-uuid* *root-device-uuid*) "/boot" "")))
