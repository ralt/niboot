(in-package #:niboot)

(setf (logical-pathname-translations "NIBOOT")
      '(("ROOT;*.*" #P"/original_root/var/lib/niboot/*.*")
        ("ARCHIVES;*.*" #P"/original_root/var/lib/niboot/archives/*.*")
        ("IMAGES;**;*.*" #P"/original_root/var/lib/niboot/images/**/*.*")))

(init)
