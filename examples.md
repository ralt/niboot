# Examples

Here are some examples of root filesystems made by [isoostrap][0] with
the configuration to make them work on niboot.

## Debian 9/Gnome Desktop

```
shell> cat > post-install.sh <<EOF
#!/usr/bin/env bash

mkdir -p /var/lib/tmp /var/lib/udisks2 /var/tmp
ln -sfn /proc/mounts /etc/mtab
EOF
shell> isoostrap --iso debian-9.1.0-amd64-netinst.iso --output debian-gnome.tar.gz --isoo debian -- --no-kernel --tasksel standard,laptop,gnome-desktop --packages systemd,network-manager,strace --user-username ralt --post-install post-install.sh --cache deb-cache
shell> niboot create --volume-mapping /niboot/debian-gnome/logs:/var/log --volume-mapping /niboot/debian-gnome/home:/home/ralt --temporary-volume /var/lib/systemd --temporary-volume /var/lib/gdm3 --temporary-volume /var/lib/AccountsService/users --temporary-volume /var/lib/NetworkManager --temporary-volume /var/lib/tmp --temporary-volume /var/cache/man --temporary-volume /var/spool/anacron --temporary-volume /var/tmp --temporary-volume /var/lib/colord --temporary-file /etc/mtab debian-gnome debian-gnome.tar.gz
shell> niboot setup
```


  [0]: https://gitlab.com/ralt/isoostrap
