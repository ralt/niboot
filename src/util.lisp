(defpackage #:niboot/util
  (:use :cl)
  (:export is-compressed-tar-archive
           sha256-sum
           getuid
           symlink
           mount
           umount
           bind-mount
           modprobe
           with-temporary-directory-and-chdir
           current-kernel-version
           strip-from-pathname-directory
           absolute-to-relative
           mounts
           where
           what
           block-device-property
           chmod
           find-library-path
           output
           read-niboot-kernel-parameters
           exec
           swapon
           parse-fstab
           fstab-type
           fstab-device
           lstat-check
           kernel-cmdline))

(in-package #:niboot/util)

(defun is-compressed-tar-archive (path)
  (handler-case
      (when (ppcre:scan
             "POSIX tar archive"
             (with-output-to-string (s)
               (uiop:run-program `("file" "-z" ,path) :output s)
               s))
        t)
    (error () nil)))

(defun cat (&rest args)
  (apply #'concatenate 'string args))

(defun sha256-sum (path)
  (ppcre:register-groups-bind (hash)
      ("^(\\w+)\\s.*$" (with-output-to-string (s)
                         (uiop:run-program `("sha256sum" ,path) :output s)
                         s))
    hash))

(cffi:defcfun ("getuid" getuid) :unsigned-int)

(cffi:defcfun ("symlink" symlink) :int
  (target :string)
  (linkpath :string))

(defun mount (what who where)
  (uiop:run-program `("/bin/busybox" "mount" "-t" ,what ,who ,where)
                    :output t
                    :error-output *error-output*))

(defun umount (what)
  (uiop:run-program `("/bin/busybox" "umount" ,what)
                    :output t
                    :error-output *error-output*))

(defun bind-mount (from to)
  (uiop:run-program `("/bin/busybox" "mount" "--bind" ,from ,to)))

(defun modprobe (module)
  (uiop:run-program `("/bin/modprobe" ,module)
                    :output t
                    :error-output *error-output*))

(cffi:defcfun ("mkdtemp" %mkdtemp) :string
  (template :string))

(defun mkdtemp (&optional (prefix (uiop:temporary-directory)))
  (pathname
   (cat
    (%mkdtemp
     (namestring
      (uiop:merge-pathnames* "niboot.XXXXXX" prefix)))
    "/")))

(defmacro with-temporary-directory-and-chdir (var &body body)
  (let ((original-dir (gensym)))
    `(let ((,original-dir (uiop:getcwd))
           (,(first var) ,(if (second var)
                              `(mkdtemp (second var))
                              `(mkdtemp))))
       (unwind-protect
            (progn
              (uiop:chdir ,(first var))
              (progn ,@body))
         (unwind-protect
              (uiop:chdir ,original-dir)
           (uiop:delete-directory-tree ,(first var)
                                       :validate t))))))

(defun current-kernel-version ()
  (string-trim '(#\Newline)
               (with-output-to-string (s)
                 (uiop:run-program '("uname" "-r") :output s)
                 s)))

(defun read-mounts ()
  (with-open-file (f "/proc/mounts")
    (let ((buf (make-array 4096 :initial-element #\Newline)))
      (read-sequence buf f)
      (mapcar (lambda (v) (coerce v 'string))
              (remove-if (lambda (v) (= (length v) 0))
                         (ppcre:split #\Newline buf))))))

(defclass mount ()
  ((what :reader what :initarg :what)
   (where :reader where :initarg :where)
   (type :reader mount-type :initarg :type)
   (options :reader mount-options :initarg :options)))

(defmethod print-object ((mount mount) stream)
  (format stream
          "#<MOUNT WHAT=~a WHERE=~a TYPE=~a>"
          (what mount)
          (namestring (where mount))
          (mount-type mount)))

(defun parse-mounts (mounts)
  (mapcar
   (lambda (mount)
     (let ((parts (ppcre:split #\Space mount)))
       (make-instance 'mount
                      :what (first parts)
                      :where (make-pathname
                              :directory `(:absolute ,@(rest (ppcre:split #\/ (second parts)))))
                      :type (third parts)
                      :options (fourth parts))))
   mounts))

(defun mounts ()
  (parse-mounts (read-mounts)))

(defun strip-from-pathname-directory (prefix path)
  (make-pathname
   :directory (append
               '(:absolute)
               (nthcdr (length (pathname-directory prefix)) (pathname-directory path)))
   :name (pathname-name path)
   :type (pathname-type path)))

(defun absolute-to-relative (pathname)
  (make-pathname :directory `(:relative ,@(rest (pathname-directory pathname)))
                 :name (pathname-name pathname)
                 :type (pathname-type pathname)))

(defun block-device-property (property device)
  "Fetch a block device property"
  (string-right-trim
   '(#\Newline)
   (with-output-to-string (s)
     (uiop:run-program `("lsblk"
                         "--raw"
                         "--noheadings"
                         "--output" ,property
                         ,(namestring device))
                       :output s)
     s)))

(cffi:defcfun ("chmod" %chmod) :int
  (pathname :string)
  (mode :unsigned-int))

(defun chmod (pathname mode)
  (%chmod (namestring pathname) mode))

(defun find-library-path (library)
  (first
   (last
    (first
     (remove-if-not
      (lambda (items)
        (string= (first items) library))
      (mapcar (lambda (line)
                (ppcre:split #\Space (string-trim '(#\Space #\Tab) line)))
              (ppcre:split #\Newline
                           (with-output-to-string (s)
                             (uiop:run-program '("ldconfig" "-p")
                                               :output s)
                             s))))))))

(defun output (text &optional (where #p"/proc/self/fd/0"))
  "A more efficient way to write text.

FORMAT T ...  in ECL, at least, writes byte after byte."
  (with-open-file (f where :direction :output :element-type '(unsigned-byte 8))
    (write-sequence (babel:string-to-octets text) f)))

(defun read-niboot-kernel-parameters ()
  (let* ((cmdline (with-open-file (f #p"/proc/cmdline") (read-line f)))
         (parts (ppcre:split #\Space cmdline)))
    (reduce (lambda (ret part)
              (multiple-value-bind (match parameter)
                  (ppcre:scan-to-strings "^niboot\.(.+)$" part)
                (if match
                    (append ret (list (elt parameter 0)))
                    ret)))
            parts
            :initial-value nil)))

(cffi:defcfun ("execv" execv) :int
  (path :string)
  (argv (:pointer :string)))

(defun exec (&rest args)
  (execv (cffi:foreign-string-alloc (first args))
         (cffi:foreign-alloc '(:pointer :string)
                             :initial-contents (mapcar #'cffi:foreign-string-alloc args)
                             :null-terminated-p t)))

(ffi:clines "#include <sys/types.h>")
(ffi:clines "#include <sys/stat.h>")
(ffi:clines "#include <unistd.h>")

(defun lstat-check (path)
  (= (ffi:c-inline ((ffi:convert-to-cstring path)) (:cstring)
                   :int "struct stat statbuf; @(return) = lstat(#0, &statbuf);"
                   :one-liner nil
                   :side-effects nil)
     0))

(defclass fstab-entry ()
  ((fstab-type :initarg :type :reader fstab-type)
   (fstab-device :initarg :device :reader fstab-device)))

(defmethod print-object ((entry fstab-entry) stream)
  (format stream "#<FSTAB-ENTRY TYPE=~a DEVICE=~a>"
          (fstab-type entry)
          (fstab-device entry)))

(defun parse-fstab (path)
  (with-open-file (f path)
    (loop
       :for line = (read-line f nil 'eof)
       :until (eq line 'eof)
       :unless (or (ppcre:scan "^\\s*#.*" line)
                   (string= (string-trim '(#\Space #\Tab) line) ""))
       :collect (let ((parts (ppcre:split "\\s+" line)))
                  (make-instance 'fstab-entry
                                 :type (third parts)
                                 :device (first parts))))))

(defun swapon (device)
  (uiop:run-program `("/bin/busybox" "swapon" ,device)))

(defun kernel-cmdline ()
  (uiop:read-file-line #P"/proc/cmdline"))
