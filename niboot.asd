(defsystem #:niboot
  :description "A smarter bootloader" ;; totally modest
  :license "GPLv3"
  :author "Florian Margaine <florian@margaine.com>"
  :depends-on (:alexandria
	       :babel
	       :cffi
               :cl-ppcre
               :command-line-arguments
	       :jsown
               :uiop)
  :components ((:module
		"src"
		:components
		((:file "niboot" :depends-on ("database"
					      "boot"
					      "util"))
		 (:file "util")
		 (:file "database" :depends-on ("util"))
		 (:file "boot" :depends-on ("util"
                                            "database"))
		 #+niboot-cli (:file "cli" :depends-on ("niboot"))
		 #+niboot-init (:file "init" :depends-on ("niboot"))))))
