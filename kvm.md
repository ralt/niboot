# kvm setup

To be able to easily test the custom image, it is much easier to use a
qemu-kvm VM rather than doing it on the real hardware. Here are a
couple of commands to make this easy.

First off, create an empty disk image. It will be a sparse file, so
you can choose a big disk size and only the necessary disk space will
be used.

```shell
qemu-img create -f qcow2 my-disk.mg 20G
```

You then need to boot on a previously-downloaded ISO of your favorite
distribution, to install the OS on your new disk image.

```shell
qemu-system-x86_64 \
	-enable-kvm \
	-cpu host \
	-m 512 \
	-smp 4 \
	-hda my-disk.img \
	-cdrom debian-9.1.0-amd64-netinst.iso \
	-boot d
```

Install the OS to your convenience.

After that is done, shutdown the VM (Ctrl-c in the terminal works), and restart it with other arguments:

```shell
qemu-system-x86_64 \
	-fsdev local,security_model=passthrough,id=fsdev0,path=$PWD \
	-device virtio-9p-pci,id=fs0,fsdev=fsdev0,mount_tag=hostfs \
	-enable-kvm \
	-cpu host \
	-m 4096 \
	-smp 4 \
	-redir tcp:2222::22 \
	my-disk.img
```

If you want to disable the X window of qemu, you can use `-nographic`.

EFI snippets:

```shell
qemu-img create -f qcow2 my-efi-disk.mg 20G
```

```shell
qemu-system-x86_64 \
	-enable-kvm \
	-cpu host \
	-m 512 \
	-smp 4 \
	-hda my-efi-disk.img \
	-cdrom debian-9.1.0-amd64-netinst.iso \
    -bios /usr/share/edk2/ovmf/OVMF_CODE.fd \
	-boot d
```

```shell
qemu-system-x86_64 \
	-fsdev local,security_model=passthrough,id=fsdev0,path=$PWD \
	-device virtio-9p-pci,id=fs0,fsdev=fsdev0,mount_tag=hostfs \
	-enable-kvm \
	-cpu host \
	-m 4096 \
	-smp 4 \
	-redir tcp:2222::22 \
    -bios /usr/share/edk2/ovmf/OVMF_CODE.fd \
	-hda my-efi-disk.img
```

Once you are in the VM, you can mount the shared folder with:

```shell
# 'hostfs' there is the mount_tag previously given.
mount hostfs /somewhere -t 9p -o trans=virtio,version=9p2000.L
```

Enjoy!
