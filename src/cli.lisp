(in-package #:niboot)

(setf (logical-pathname-translations "NIBOOT")
      '(("ROOT;*.*" #P"/var/lib/niboot/*.*")
        ("ARCHIVES;*.*" #P"/var/lib/niboot/archives/*.*")
        ("IMAGES;**;*.*" #P"/var/lib/niboot/images/**/*.*")
        ("TMP;**;*.*" #P"/var/lib/niboot/tmp/**/*.*")))

(cli)

(ext:quit 0)
