#!/bin/bash

dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

LD_LIBRARY_PATH="$dir"/vendor/ecl/build/ "$dir"/vendor/ecl/build/bin/ecl -norc --eval "(setf (logical-pathname-translations \"SYS\") '((#P\"SYS:**;*.*\" #P\"${dir}/vendor/ecl/build/**/*.*\")))" --load "$dir"/init.lisp "$@"
