(declaim (optimize
	  (debug 0)
	  (safety 0)
	  (speed 3)
	  (space 3)
	  (compilation-speed 0)))

(require 'asdf)

(asdf:ensure-source-registry `(:source-registry
			       (:directory ,(uiop:getcwd))
			       (:directory ,(uiop:merge-pathnames*
					     #p"vendor/jsown"
					     (uiop:getcwd)))
			       (:tree ,(uiop:merge-pathnames*
					#p"quicklocal/dists/quicklisp/software/"
					(uiop:getcwd)))
			       :ignore-inherited-configuration))

(setf asdf::*user-cache* (uiop:merge-pathnames*
                          (make-pathname :directory (list :relative (uiop:implementation-identifier)))
                          (uiop:merge-pathnames* #p"cache/" (uiop:getcwd))))
(asdf:initialize-output-translations '(:output-translations
				       :ignore-inherited-configuration
				       :enable-user-cache))
