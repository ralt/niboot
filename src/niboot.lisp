(defpackage #:niboot
  (:use :cl))

(in-package #:niboot)

(defvar *build-prefix* #.(uiop:parse-native-namestring (uiop:getenv "PREFIX")))

(defvar *commands* (make-hash-table :test #'equal))

(defmacro defcommand (name args &body body)
  `(progn
     (setf (gethash (string-downcase (symbol-name ',name))
                    *commands*)
           (lambda ,args
             (declare (ignorable ,@args))
             (prog ()
                ,@body)))))

(defun wrap (message)
  (niboot/util:output (format nil "~a

Report bugs to <https://gitlab.com/ralt/niboot>.
" message)))

(defun args-usage (spec)
  (with-output-to-string (s)
    (command-line-arguments:show-option-help spec :stream s)
    s))

(defvar *create-args*
  '((("help") :type boolean :optional t :documentation "show this usage")
    (("volume-mapping") :type string :list t :optional t :documentation "volume mapping")
    (("temporary-volume") :type string :list t :optional t :documentation "temporary volume")
    (("temporary-file") :type string :list t :optional t :documentation "temporary file")))

(defcommand create (arguments)
  (command-line-arguments:handle-command-line
   *create-args*
   (lambda (args &key help volume-mapping temporary-volume temporary-file)
     (when help (return (wrap (format nil "Usage: niboot create [OPTION]... IMAGE SOURCE

Create a niboot image named IMAGE, based on SOURCE.

~a" (args-usage *create-args*)))))
     (destructuring-bind (image source) args
       (unless (and (probe-file source)
                    (niboot/util:is-compressed-tar-archive source))
         (return
           (format *error-output* "~a is not a compressed tar archive.~%" source)))
       (unless (= (niboot/util:getuid) 0)
         (return
           (format *error-output* "You need to be root to run this command.~%")))
       (when (niboot/database:exists image)
         (return
           (format *error-output* "~a already exists.~%" image)))
       (niboot/database:create image source
                               (mapcar (lambda (mapping)
                                         (destructuring-bind (source destination)
                                             (ppcre:split ":" mapping)
                                           (make-instance 'niboot/database:volume-map
                                                          :source-path source
                                                          :destination-path destination)))
                                       volume-mapping)
                               temporary-volume
                               temporary-file)))
   :command-line arguments
   :rest-arity t))

(defvar *update-args*
  '((("help") :type boolean :optional t :documentation "show this usage")))

(defcommand update (arguments)
  (command-line-arguments:handle-command-line
   *update-args*
   (lambda (args &key help)
     (when help (return (wrap (format nil "Usage: niboot update [OPTION]... IMAGE SOURCE

Update the niboot image named IMAGE, based on SOURCE.

~a" (args-usage *update-args*)))))
     (destructuring-bind (image source) args
       (niboot/database:update image source)))
   :command-line arguments
   :rest-arity t))

(defvar *delete-args*
  '((("help") :type boolean :optional t :documentation "show this usage")))

(defcommand delete (arguments)
  (command-line-arguments:handle-command-line
   *delete-args*
   (lambda (args &key help)
     (when help (return (wrap (format nil "Usage: niboot delete [OPTION]... IMAGE

Delete the niboot image named IMAGE.

~a" (args-usage *delete-args*)))))
     (let ((image (first args)))
       (niboot/database:delete-image image)))
   :command-line arguments
   :rest-arity t))

(defvar *setup-args*
  '((("help") :type boolean :optional t :documentation "show this usage")))

(defcommand setup (arguments)
  (command-line-arguments:handle-command-line
   *setup-args*
   (lambda (args &key help)
     (when help (return (wrap (format nil "Usage: niboot setup [OPTION]...

Setup your computer boot options to support niboot.

~a" (args-usage *setup-args*)))))
     (niboot/boot:setup *build-prefix*))
   :command-line arguments
   :rest-arity t))

;;; Entrypoint for the 'niboot' CLI.
(defun cli ()
  (let ((argv (rest (si:command-args))))
    (cond
      ((multiple-value-bind (_ present-p)
           (gethash (first argv) *commands*)
         (declare (ignore _))
         present-p)
       (funcall (gethash (first argv) *commands*) (rest argv)))
      (t (wrap (format nil "Usage: niboot COMMAND [OPTION]...
A smarter bootloader.
Type 'niboot COMMAND --help' for more information about the command.

Commands:
~t~tcreate~t~tcreate a new niboot image
~t~tupdate~t~tupdate a niboot image
~t~tdelete~t~tdelete a niboot image
~t~tsetup~t~t~tsetup niboot bootloader"))))))

;;; Entrypoint for the initramfs's init.
(defun init ()
  (let ((uuids (jsown:parse (uiop:read-file-string #p"uuids"))))
    (niboot/boot:minimal-setup uuids)

    (loop
       (block main-loop
         (let ((parameters (niboot/util:read-niboot-kernel-parameters))
               (images (niboot/database:list-images)))
           (niboot/util:output (format nil "Which image do you want to boot on?~%"))
           (loop
              for i from 0 below (length images)
              do (niboot/util:output (format nil "[~a] ~a~%" (1+ i) (niboot/database:name
                                                                     (nth i images)))))

           (niboot/util:output (format nil "[~a] debug~%" (1+ (length images))))

           (handler-case
               (progn
                 (niboot/util:output "Choose wisely: ")
                 (force-output t)
                 (let ((choice (1- (parse-integer (read-line)))))
                   (when (= choice (length images))
                     (si:top-level)
                     (return-from main-loop))
                   (if (nth choice images)
                       (return-from init
                         (niboot/boot:on (nth choice images) uuids))
                       (niboot/util:output (format nil "~a not found in the list.~%" (1+ choice))))))
             (parse-error () nil)
             (error (e) (niboot/util:output (format nil "~a~%" e)))))))))
