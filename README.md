% NIBOOT(1) Niboot User Manual
% Florian Margaine <florian@margaine.com>
% August 6, 2017

# NAME

niboot - a smarter bootloader

# SYNOPSIS

niboot COMMAND ARGUMENT...

# DESCRIPTION

niboot lets you manage multiple root filesystems and choose which one
to boot on. The root filesystem will be mounted as read-only, and
various folders will be re-mounted as read-write, to have a usable
system.

# COMMANDS

## niboot help

Display the help screen for niboot.

## niboot create help

Display the help screen for niboot create.

## niboot create IMAGE SOURCE

Create an image named IMAGE based on SOURCE.

SOURCE must be a compressed tar archive of the root filesystem.

## niboot update help

Display the help screen for niboot update.

## niboot update IMAGE SOURCE

Update the image named IMAGE based on SOURCE.

SOURCE must be a compressed tar archive of the root filesystem. The
new image will be based on the previous one, using reflinks or hard
links.

## niboot delete help

Display the help screen for niboot delete.

## niboot delete IMAGE

Delete the image named IMAGE.

This is an irreversible action. Until you re-create it again.

## niboot setup help

Display the help screen for niboot setup.

## niboot setup [KERNEL-VERSION]

Setup your computer boot options to support niboot.

Optionally, provide the kernel version for which to build niboot.

# SEE ALSO

The niboot source code and all documentation may be downloaded
from <https://gitlab.com/ralt/niboot>.
