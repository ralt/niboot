(defpackage #:niboot/database
  (:use :cl)
  (:export exists
           create
           delete-image
           update
           list-images
           name
           hashes
           archive
           volume-map
           volumes-maps
           source-path
           temporary-volumes
           temporary-files
           destination-path))

(in-package #:niboot/database)

(defun mp (&rest args)
  "A shortcut for uiop:merge-pathnames*"
  (apply #'uiop:merge-pathnames* args))

(defun exists (image)
  (not (eq (probe-file (mp (make-pathname :name image :type "json") "NIBOOT:ROOT;"))
           nil)))

(defun generate-squashfs (image)
  (let ((image-directory (translate-logical-pathname
                          (mp (make-pathname :directory `(:relative ,(first (hashes image))))
                              "NIBOOT:IMAGES;"))))
    (unless (niboot/util:lstat-check (namestring (mp #p"sbin/init" image-directory)))
      ;; Using lstat(2) so that even an invalid symlink works.
      (uiop:delete-directory-tree image-directory :validate t)
      (error "Unable to find an init binary in the image. Aborting."))

    (let ((image-fstab (mp (make-pathname :directory '(:relative "etc")
                                          :name "fstab"
                                          :type nil)
                           image-directory)))
      (when (probe-file image-fstab)
        (delete-file image-fstab))
      (niboot/util:symlink "/run/.niboot/fstab"
                           (namestring image-fstab)))

    (let ((image-mtab (mp (make-pathname :directory '(:relative "etc")
                                         :name "mtab"
                                         :type nil)
                          image-directory)))
      (when (probe-file image-mtab)
        (delete-file image-mtab))
      (niboot/util:symlink "/proc/self/mounts"
                           (namestring image-mtab)))

    (dolist (folder (list (make-pathname :directory '(:relative "lib" "modules"))
                          (make-pathname :directory '(:relative "usr" "lib" "modules"))))
      (let ((modules-folder (mp folder image-directory)))
        (when (probe-file modules-folder)
          (uiop:delete-directory-tree modules-folder :validate t))
        (ensure-directories-exist modules-folder)))

    (dolist (volume-map (volumes-maps image))
      ;; Absolute path needs to be made relative
      (ensure-directories-exist
       (mp (subseq (destination-path volume-map) 1) image-directory)))

    (dolist (temporary-file (temporary-files image))
      ;; "temporary files" work by creating a symlink at build time
      ;; (i.e. now), and having niboot's init make sure the folder
      ;; where the file exists is a tmpfs at runtime.
      (let ((in-image-temporary-file (mp (subseq temporary-file 1) image-directory)))
        (when (probe-file in-image-temporary-file)
          (delete-file in-image-temporary-file))
        (niboot/util:symlink (namestring (mp (subseq temporary-file 1) #p"/run/.niboot/"))
                             (namestring in-image-temporary-file))))

    (ensure-directories-exist (mp #p"boot/" image-directory))

    (uiop:run-program `("mksquashfs"
                        ,(namestring image-directory)
                        ,(namestring (translate-logical-pathname (mp (name image) "NIBOOT:ARCHIVES;")))
                        ;; Not all kernels support other than gzip
                        "-comp" "gzip"
                        ;; We want smallest on-disk
                        "-Xcompression-level" "9"
                        ;; Bloat
                        "-no-progress"))))

(defun create (image source volumes-maps temporary-volumes temporary-files)
  (uiop:ensure-all-directories-exist '("NIBOOT:ROOT;"
                                       "NIBOOT:IMAGES;"
                                       "NIBOOT:ARCHIVES;"))
  (let* ((source-hash (niboot/util:sha256-sum source))
         (db-image (make-instance 'image
                                  :name image
                                  :hashes (list source-hash)
                                  :volumes-maps volumes-maps
                                  :temporary-volumes (append (list "/tmp" "/run")
                                                             temporary-volumes)
                                  :temporary-files temporary-files))
         (image-directory (translate-logical-pathname
                           (mp (make-pathname :directory `(:relative ,source-hash))
                               "NIBOOT:IMAGES;"))))
    (ensure-directories-exist image-directory)
    (uiop:run-program `("tar" "xf" ,source "-C" ,(namestring image-directory)) :output t :error-output t)

    (generate-squashfs db-image)

    (write-image db-image)))

(defun write-image (image)
  (with-open-file (f (translate-logical-pathname
                      (mp (make-pathname :name (name image)
                                         :type "json")
                          "NIBOOT:ROOT;"))
                     :direction :output
                     :if-exists :overwrite
                     :if-does-not-exist :create)
    (write-string (jsown:to-json `(:obj
                                   (:hashes . ,(hashes image))
                                   (:volumes-maps
                                    . (:obj
                                       ,@(mapcar (lambda (volume-map)
                                                   (list*
                                                    (source-path volume-map)
                                                    (destination-path volume-map)))
                                                 (volumes-maps image))))
                                   (:temporary-volumes . ,(temporary-volumes image))
                                   (:temporary-files . ,(temporary-files image))))
                  f)))

(defun update (image source)
  ;; 1. Load the existing image definition
  ;; 2. Get a hash of the source
  ;; 3. Copy-paste hardlinks of the latest source in the image to NIBOOT:IMAGES;<new source hash>
  ;; 4. Extract the new source to NIBOOT:TMP;<new source hash>
  ;; 5. Run rsync between NIBOOT:TMP;<new source hash> and NIBOOT:IMAGES;<new source hash>
  ;; 6. Cleanup NIBOOT:TMP
  ;; 7. Generate a squashfs and update the image definition
  ;;
  ;; This way is the sanest I can think of the minimize disk usage
  ;; over time. A better way would probably be to use archivemount or
  ;; similar, but I haven't heard good things about it. Open to
  ;; improving.
  ;;
  ;; @TODO error handling all over database.lisp. It's kind of
  ;; shameful in the current state.
  (uiop:ensure-all-directories-exist '("NIBOOT:ROOT;"
                                       "NIBOOT:IMAGES;"
                                       "NIBOOT:ARCHIVES;"
                                       "NIBOOT:TMP;"))
  (let* ((old-image (load-image-file (translate-logical-pathname
                                      (mp (make-pathname :name image :type "json")
                                          "NIBOOT:ROOT;"))))
         (old-image-directory (translate-logical-pathname
                               (mp (make-pathname :directory `(:relative ,(first (hashes old-image))))
                                   "NIBOOT:IMAGES;")))
         (source-hash (niboot/util:sha256-sum source))
         (db-image (make-instance 'image
                                  :name image
                                  :hashes (append (list source-hash) (hashes old-image))
                                  :volumes-maps (volumes-maps old-image)
                                  :temporary-volumes (temporary-volumes old-image)
                                  :temporary-files (temporary-files old-image)))
         (image-directory (translate-logical-pathname
                           (mp (make-pathname :directory `(:relative ,source-hash))
                               "NIBOOT:IMAGES;")))
         (tmp-directory (translate-logical-pathname
                         (mp (make-pathname :directory `(:relative ,source-hash))
                             "NIBOOT:TMP;"))))

    (uiop:run-program `("cp"
                        "-al"
                        ,(namestring old-image-directory)
                        ,(namestring image-directory)))

    (ensure-directories-exist tmp-directory)

    (unwind-protect
         (progn
           (uiop:run-program `("tar" "xf" ,source "-C" ,(namestring tmp-directory)))
           (uiop:run-program `("rsync"
                               "--archive"
                               "--hard-links"
                               "--delete-after"
                               ,(namestring tmp-directory)
                               ,(namestring image-directory))))
      (uiop:delete-directory-tree tmp-directory :validate t))

    (generate-squashfs db-image)

    (write-image db-image)))

(defun delete-image (image)
  "Delete the image."
  (let* ((image-file (mp (make-pathname :name image :type "json")
                         "NIBOOT:ROOT;"))
         (db-image (load-image-file (translate-logical-pathname image-file))))
    (dolist (hash (hashes db-image))
      (uiop:delete-directory-tree (translate-logical-pathname
                                   (mp (make-pathname :directory `(:relative ,hash))
                                       "NIBOOT:IMAGES;"))
                                  :validate t))
    (delete-file (mp image "NIBOOT:ARCHIVES;"))
    (delete-file image-file)))

(defun list-images ()
  (remove-if-not
   #'identity
   (mapcar (lambda (image-file)
             (handler-case
                 (load-image-file image-file)
               ;; Protect against broken JSON files.
               (error ()
                 nil)))
           (directory (mp (make-pathname :name :wild
                                         :type "json")
                          "NIBOOT:ROOT;")))))

(defclass image ()
  ((name :initarg :name :reader name)
   (hashes :initarg :hashes :reader hashes)
   (volumes-maps :initarg :volumes-maps :reader volumes-maps)
   (temporary-volumes :initarg :temporary-volumes :reader temporary-volumes)
   (temporary-files :initarg :temporary-files :reader temporary-files)))

(defclass volume-map ()
  ((source-path :initarg :source-path :reader source-path)
   (destination-path :initarg :destination-path :reader destination-path)))

(defun archive (image)
  (translate-logical-pathname
   (mp (name image) "NIBOOT:ARCHIVES;")))

(defun path-to-pathname (path)
  (make-pathname :directory
                 (append '(:relative)
                         (remove-if
                          (lambda (part)
                            (string= part ""))
                          (ppcre:split "/" path)))))

(defun load-image-file (image-file)
  (let ((raw-image (jsown:parse (uiop:read-file-string image-file)))
        (volumes-maps))
    (jsown:do-json-keys (source destination) (jsown:val raw-image :volumes-maps)
      (push (make-instance 'volume-map
                           :source-path (path-to-pathname source)
                           :destination-path (path-to-pathname destination))
            volumes-maps))
    (make-instance 'image
                   :name (pathname-name image-file)
                   :hashes (jsown:val raw-image :hashes)
                   :volumes-maps volumes-maps
                   :temporary-volumes (jsown:val raw-image :temporary-volumes)
                   :temporary-files (jsown:val raw-image :temporary-files))))
